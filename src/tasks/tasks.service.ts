import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { TaskRepository } from './task.repository';
import { Task } from './entities/task.entity';
import { TaskStatus } from './task-status.enum';
import { DeleteResult, UpdateResult } from 'typeorm';
import { User } from '../auth/user.entity';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(TaskRepository)
    private taskRepository: TaskRepository,
  ) {
  }

  async getTasks(filterDto: GetTasksFilterDto, user: User): Promise<Task[]> {
    return await this.taskRepository.getTasks(filterDto, user);
  }

  async getTaskById(id: number, user: User): Promise<Task> {
    const taskFound = await this.taskRepository.findOne({ where: { id, userId: user.id }});

    if (!taskFound) {
      throw new NotFoundException(`Task with ID "${id}" not found`);
    }

    return taskFound;
  }

  async createTask(createTaskDto: CreateTaskDto, user: User): Promise<Task> {
    return await this.taskRepository.createTask(createTaskDto, user);
  }

  async updateTaskStatus(id: number, status: TaskStatus, user: User): Promise<UpdateResult> {
    const updateResult = await this.taskRepository.update({ id, userId: user.id }, { status });

    if (updateResult.affected === 0) {
      throw new NotFoundException(`Task with ID: "${id}" not found`);
    }

    return updateResult;
  }

  async deleteTask(id: number, user: User): Promise<DeleteResult> {
    const deleteResult = await this.taskRepository.delete({id, userId: user.id});

    if (deleteResult.affected === 0) {
      throw new NotFoundException(`Task with ID: "${id}" not found`);
    }

    return deleteResult;
  }
}
