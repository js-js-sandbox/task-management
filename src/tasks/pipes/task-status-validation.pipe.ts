import { BadRequestException, PipeTransform } from '@nestjs/common';
import { TaskStatus } from '../task-status.enum';
import { Validator } from 'class-validator';

export class TaskStatusValidationPipe implements PipeTransform {
  private validator = new Validator();

  async transform(value: any) {
    value = value.toUpperCase();
    const isValid = this.validator.isEnum(value, TaskStatus);
    if (!isValid) {
      throw new BadRequestException(`"${value}" is an invalid status`);
    }
    return value;
  }
}
